// HGCommon project HGCommon.go
package HGCommon

import (
	"strconv"
	"strings"
)

//一般的浮点型转字符串都会有出现.0的情况，比如 18-》18.000000。不美观，鸡肋
//本函数可以根据精度将无用的.0去掉。本函数不支持有效位数的设置
//precision 有效精度
func FloatToString(num float64, precision int) string {
	strRet := strconv.FormatFloat(num, 'f', precision, 64)
	strRet = strings.Trim(strRet, "0")
	strRet = strings.Trim(strRet, ".")
	return strRet
}
